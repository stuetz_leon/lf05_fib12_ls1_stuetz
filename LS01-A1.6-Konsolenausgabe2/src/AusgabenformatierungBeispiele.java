
public class AusgabenformatierungBeispiele {
	
	public static void main (String[] args) 
	{
		
		// Ganzzahl
		
		System.out.printf("|%+-10d%+10d|\n", 123456, -123456);
		
		// Kommazahl
		System.out.printf("|%+-10.2f|", 12.123456789);
		
		//Zeichenkette
		System.out.printf("|%-10.3s|\n\n", "Max Mustermann");
		
		//System.out.printf("Name: %-10sAlter:-8d   Gewicht:%-10.2f2", "Max", 18, 80.5);
		
		
		
		
	}
}
