public class Konsolenausgabe {
	
	public static void main (String[] agrs) 
	{
		
		//Aufgabe 1
		System.out.println(" ______________________________________________________ ");
		System.out.println("|                                                      |");
		System.out.println("|                      Aufgabe 1                       |");
		System.out.println("|______________________________________________________|");
		System.out.println();
		
		
		String  textEins = "Person 1: \"Hallo, ich bin ein Tiger :)\"\n";
		String textZwei = "Person 2: \"Hallo, ich bin kein Tiger :(\"";
		
		System.out.print(textEins + textZwei);
		System.out.println();
		//Bei der println Anweisung wird ein Zeilenausbruch nach der Ausgabe gemacht, bei print nicht.
		
		
		
		//Aufgabe 2
		System.out.println(" ______________________________________________________ ");
		System.out.println("|                                                      |");
		System.out.println("|                      Aufgabe 2                       |");
		System.out.println("|______________________________________________________|");
		System.out.println();

		
		//String s = "*************";
		//System.out.printf( "%7,s 	*\n" );
		
		
		String s01 = "      *      ";
		String s02 = "     ***     ";
		String s03 = "    *****    ";
		String s04 = "   *******   ";
		String s05 = "  *********  ";
		String s06 = " *********** ";
		String s07 = "*************";
		String s08 = "     ***     ";
		String s09 = "     ***     ";
		
		
		System.out.println( s01 + "\n" + s02 + "\n" + s03 + "\n" + s04 + "\n" + s05 + "\n" + 
							s06 + "\n" + s07 + "\n" + s08 + "\n" + s09 + "\n" );
		
		System.out.println( "      *      \n" +
							"     ***     \n" +
							"    *****    \n" +
							"   *******   \n" +
							"  *********  \n" +
							" *********** \n" +
							"*************\n" +
							"     ***     \n" +
							"     ***     \n");
		
		
		//Aufgabe 3
				System.out.println(" ______________________________________________________ ");
				System.out.println("|                                                      |");
				System.out.println("|                      Aufgabe 3                       |");
				System.out.println("|______________________________________________________|");
				System.out.println();
				
				
				double z1 = 22.4234234;
				double z2 = 22.4234234;
				double z3 = 4.0;
				double z4 = 1000000.551;
				double z5 = 97.34;
				
				System.out.printf( "%.2f \n" , z1);
				System.out.printf( "%.2f \n" , z2);
				System.out.printf( "%.2f \n" , z3);
				System.out.printf( "%.2f \n" , z4);
				System.out.printf( "%.2f \n" , z5);

	}
	
}
