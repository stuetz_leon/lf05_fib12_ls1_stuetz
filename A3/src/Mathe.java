
public class Mathe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println ( hypotenuse ( 1.5, 3.0 ) );
	}
	
	public static double quadrat ( double x )
	{
		return x * x;
	}
	
	public static double hypotenuse ( double kathete1, double kathete2 )
	{
		double h = Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
		return h;
	}
}
