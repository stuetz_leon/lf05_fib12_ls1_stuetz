import java.util.Scanner;

public class Multiplikation {

    public static void main(String[] args) {
       
        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        Scanner myScanner = new Scanner(System.in);
       
        //1.Programmhinweis
        ausgebenProgrammhinweis();
        
        //4.Eingabe
        zahl1 = leseZahlEin(1);
        zahl2 = leseZahlEin(2);

        //3.Verarbeitung
        erg = multipliziereZahlen(zahl1, zahl2);

        //2.Ausgabe   
        gebeErgebnisAus(zahl1, zahl2, erg);
    }
    
    public static void ausgebenProgrammhinweis () 
    {
    	//1.Programmhinweis
        System.out.println("Hinweis: ");
        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
    }
    
    public static double leseZahlEin ( int welcheZahl )
    {
    	//4.Eingabe
    	Scanner sc = new Scanner ( System.in );
    	System.out.printf("%d. Zahl: ", welcheZahl  );
    	return sc.nextDouble();
    }
    
    public static double multipliziereZahlen ( double zahl1, double zahl2 )
    {
        //3.Verarbeitung
        return zahl1 * zahl2;
    }
    
    public static void gebeErgebnisAus ( double zahl1, double zahl2, double erg )
    {
    	//2.Ausgabe   
        System.out.println("Ergebnis der Multiplikation: ");
        System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
    }
    
}