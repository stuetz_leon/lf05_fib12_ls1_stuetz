import java.util.Scanner;
import java.util.Spliterator.OfPrimitive;

import javax.crypto.Cipher;

public class ForSchleifenn {
	
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner ( System.in );
		System.out.print("Aufgabe? ");
		int aufgabe = sc.nextInt();
		
		switch (aufgabe)
		{
			case 1: // Z�hlen
				aufgabe1();
				break;
			case 2:
				aufgabe2();
				break;
			case 3:
				aufgabe3();
				break;
			case 4: // Folgen
				folgen();
				break;
			case 9: // Treppe
				//aufgabe9();
				break;
		}
	}
	
	public static void aufgabe1 () // Z�hlen
	{
		Scanner sc = new Scanner ( System.in );
		System.out.print("n = ");
		int n   = sc.nextInt();
		int sum = 0;
		for ( int i = 1; i <= n; i++ )
		{
			sum += i;
		}
		System.out.println("Summe: " + sum );
		
		for ( int i = n; i > 0; i-- )
		{
			sum += i;
		}
		System.out.println("Summe: " + sum );
	}
	
	public static void aufgabe2 () //Summe
	{
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");
		int n = sc.nextInt();
		int a, b, c;
		a = b = c = 0;
	
		//A
		for ( int i = 1; i <= n; i++ )
		{
			a += i;
		}
		
		//B
		for ( int i = 2; i <= n*2; i+=2 )
		{
			b += i;
		}
		
		//B mit while
		int bi   = 0; 
		int bSum = 0;
		while ( bi <= 2*n )
		{
			bSum += bi;
			bi   +=  2;
		}
		
		//C
		for (int i = 1; i <= n*2+1; i+=2)
		{
			c += i;
		}
		
		//C mit while
		int ci   = 1;
		int cSum = 0;
		while ( ci <= 2*n+1 )
		{
			cSum += ci;
			ci   +=  2;
		}
		
		System.out.println( "Die Summe f�r A betr�gt: " + a );
		System.out.println( "Die Summe f�r B betr�gt: " + b );
		System.out.println( "Die Summe f�r B mit while betr�gt: " + bSum);
		System.out.println( "Die Summe f�r C betr�gt: " + c );
		System.out.println( "Die Summe f�r C mit while betr�gt: " + cSum );
	}
	
	public static void aufgabe3 () //Modulo
	{
		for ( int i = 1; i <= 200; i++ )
		{
			if ( i % 7 == 0 )
			{
				System.out.println( i + " ist durch 7 teilbar" );
			}
			if ( i % 5 != 0 && i % 4 == 0 )
			{
				System.out.println( i + "ist nicht durch 5 aber durch 4 teilbar" );
			}
		}
	}
	
	public static void folgen () // Folgen
	{
		Scanner sc = new Scanner ( System.in );
		System.out.println(" A ");
		// a)
		int ia = 99;
		while ( ia >= 9 )
		{	
			if ( ia == 9 )  { System.out.println ( ia ); }
			else 			{ System.out.print   ( ia + ", " ); }
			ia -= 3;
		}
		
		System.out.print("weiter?");
		String temp = sc.nextLine();
		
		
		// c)
		System.out.println("\n C ");
		int ic = 2;
		while ( ic <= 102 )
		{
			if ( ic == 102 ) { System.out.println( ic ); }
			else 			 { System.out.print  ( ic + ", " ); }
			ic += 4;
		}
		
		System.out.print("weiter?");
		temp = sc.nextLine();
		
		// e)
		System.out.println("\n E ");
		int ie = 2;
		while ( ie <= 32768 )
		{
			if ( ie == 32768 ) { System.out.println( ie ); }
			else 			   { System.out.print  ( ie + ", "); }
			ie *= 2;
		}
	}
}
