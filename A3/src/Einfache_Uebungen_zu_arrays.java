import java.util.Scanner;
import java.util.Arrays;

public class Einfache_Uebungen_zu_arrays {

	public static void main(String[] args) {
		//System.out.println(Arrays.toString(aufgabe1()));
		aufgabe4();
	}

	// Aufgabe 1
	public static int[] aufgabe1 ()
	{
		int[] zahlen = new int[10];
		for(int i = 0; i < zahlen.length; i++)
		{
			zahlen[i] = i; 
		} 
		return zahlen;
	}
	
	// Aufgabe 2
	public static int[] aufgabe2 () 
	{
		int[] ungeradeZahlen = new int[10];
		int   ungeradeZahl   = 1;
		for ( int i = 0; i < ungeradeZahlen.length; i++ )
		{
			ungeradeZahlen[i] = ungeradeZahl;
			ungeradeZahl += 2;
		}
		return ungeradeZahlen;
	}
	
	// Aufgabe 3
	public static char[] aufgabe3 ()
	{
		char[] reverse = new char[5];
		
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < reverse.length; i++)
		{
			System.out.print("Char: ");
			reverse[i] = sc.next().charAt(0);
			System.out.println();
		}
		
		int bound;
		if ( (reverse.length/2) % 1 > 0 ) 
		{
			bound = reverse.length / 2 + 1;
		}
		else
		{
			bound = reverse.length / 2;
		}
		
		for ( int i = 0; i < bound; i++ )
		{
			char temp 						= reverse[reverse.length - 1 - i]; // sieht sehr kryptisch aus, aber alternativ "4-i" zu schreiben f�nde ich bl�d
			reverse[reverse.length - 1 - i] = reverse[i];
			reverse[i] 						= temp;			
		}
		return reverse;
	}
	
	// Aufgabe 4
	public static int[] aufgabe4 ()
	{
		int[] lotto = {3, 7, 12, 18, 37, 42};
		System.out.print("[ ");
		for ( int i = 0; i < lotto.length; i++ )
		{
			System.out.print(lotto[i] + " ");
		}
		System.out.println("]");
		
		boolean is12 = false;
		boolean is13 = false;
		
		for ( int i = 0; i < lotto.length; i++ )
		{
			if ( lotto[i] == 12 )
			{
				is12 = true;
			}
			else if ( lotto[i] == 13 )
			{
				is13 = true;
			}
		}
		
		// Aufgabenteil b
		System.out.print("Die Zahl 12 ist ");
		if ( !is12 )
		{
			System.out.print("nicht ");
		}
		System.out.print("in der Ziehung enthalten.");
		
		System.out.println();
		
		System.out.print("Die Zahl 13 ist ");
		if ( !is13 )
		{
			System.out.print("nicht ");
		}
		System.out.print("in der Ziehung enthalten.");
		
		return lotto;
	}
}
