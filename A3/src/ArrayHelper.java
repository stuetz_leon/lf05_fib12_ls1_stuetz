import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

//LS05.1 A5.2 Array �bungen mit Funktionen
public class ArrayHelper {

	public static void main(String[] args) 
	{
		int[][] arr = {{1, 2},{2, 1}};
		if (isMatrixTransponiertZuSichSelbst(arr))
		{ 
			System.out.println("ja");
		}
		else 
		{
			System.out.println("nein");
		}
	}

	public static String convertArrayToString ( int[] zahlen )
	{
		String toString = String.valueOf(zahlen[0]);
		for ( int i = 1; i < zahlen.length; i++ ) 
		{
			toString += ", " + String.valueOf(zahlen[i]);
		}		
		return toString;
	}
	
	public static String convert2ArrayToString ( int[][] zahlen )
	{
		String toString = "";
		for ( int i = 0; i < zahlen.length; i++ )
		{
			for ( int j = 0; j < zahlen[i].length; j++ )
			{
				toString += " " + zahlen[i][j];
			}
			System.out.println();
		}
		return toString;
	}
	
	
	// Aufgabe 2
	public static int[] reverseIntArray ( int[] arr )
	{
		for ( int i = 0; i < arr.length/2; i++ )
		{
			//swap
			int temp = arr[i];
			arr[i]   = arr[arr.length - 1 - i];
			arr[arr.length - 1 - i] = temp;	
		}
		return arr;
	}
	
	
	// Aufgabe 3
	public static int[] reverseIntArray2 ( int[] arr )
	{
		int[] rra = new int[arr.length];
		for( int i = 0; i < arr.length; i++ )
		{
			rra[i] = arr[arr.length-1-i];
		}
		return rra;
	}

	// Aufgabe 4
	public static double[][] temperaturwerte ( int anzahlZeilen ) 
	{
		double[][] temperaturTabelle = new double[2][anzahlZeilen];
		
		double wertFahrenheit = 0.0;
		for ( int i = 0; i < anzahlZeilen; i++ )
		{
			temperaturTabelle[0][i] = wertFahrenheit;
			temperaturTabelle[1][i] = ( (5.0/9.0) * (wertFahrenheit-32.0) );
			wertFahrenheit += 10.0;
		}
		return temperaturTabelle;
	}	

	// Aufgabe 5
	public static int[][] matrixEinlesen (int n, int m)
	{
		Scanner sc = new Scanner(System.in);
		int[][] matrix = new int[n][m];
		
		for ( int i = 0; i < n; i++ )
		{
			for ( int j = 0; j < m; j++ )
			{
				System.out.print("matrix[" +  i + "][" + j + "] = ");
				int temp 		= sc.nextInt();
				matrix[i][j] 	= temp;  
			}
		}
		return matrix;
	}

	// Aufgabe 6
	public static boolean isMatrixTransponiertZuSichSelbst ( int[][] arr )
	{
		// Wichtig: Array muss eine Gr��e von n*n haben!
		boolean isTransponiertZuSichSelbst = true;
		
		for ( int i = 0; i < arr.length; i++)
		{
			for ( int j = 0; j < arr.length; j++ )
			{
				if ( arr[i][j] != arr[j][i]  )
				{
					isTransponiertZuSichSelbst = false;
					break;
				}	
			}
			if ( !isTransponiertZuSichSelbst )
			{
				break;
			}
		}
		return isTransponiertZuSichSelbst;
	}
}
