import java.nio.channels.Pipe;
import java.util.Scanner;

public class VolumenKugelBerechnen {

	public static void main ( String[] args )
	{
		double r = leseRadiusEin();
		gibErgebnisAus ( r, berechneVolumen ( r ) ); 
	}
	
	public static double leseRadiusEin ()
	{
		Scanner sc = new Scanner ( System.in  );
		System.out.print		 ( "Radius: " );
		return sc.nextDouble	 ();
	}
	
	public static double berechneVolumen ( double r )
	{
		return ( (double) 4 / (double) 3 ) * Math.PI * Math.pow(r, 3);
	}
	
	public static void gibErgebnisAus ( double r, double v )
	{
		System.out.printf("Das Volumen der Kugel mit einem Radius von %.2f betr�gt %.2f", r, v);
	}
}
