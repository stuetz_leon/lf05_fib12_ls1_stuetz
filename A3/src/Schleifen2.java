import java.util.Scanner;

import org.ietf.jgss.Oid;

public class Schleifen2 {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner ( System.in );
		System.out.println( "(7) Wettlauf" );
		System.out.println( "(8) Matrix" );
		
		int auswahl = sc.nextInt();
		switch (auswahl) 
		{
			case 7:
				wettlauf();
				break;
			case 8: 
				matrix();
				break;
		
		
		}
		
		
		matrix ();
		
	}

	
	
	public static void matrix () // Aufgabe 8
	{
		Scanner sc = new Scanner ( System.in );
		int input, n;		
		System.out.print ( "Zahl eingeben: " );
		input = sc.nextInt();
		
		for ( int i = 0; i < 10; i++ )
		{
			for ( int j = 0; j < 10; j++ )
			{
				n = 10 * i + j;
				if ( n %  input == 0 || n % 10 == input || quersumme(n) == input )
				{
					System.out.print(" * ");
				}
				else 
				{
					System.out.printf("%2d ", n );
				}
			}
		System.out.println();
		}
	}
	
	public static int quersumme ( int zahl ) 
	{
		if ( zahl <= 9 ) { return zahl; }
		return zahl % 10 + quersumme ( zahl / 10 );
	}
}
