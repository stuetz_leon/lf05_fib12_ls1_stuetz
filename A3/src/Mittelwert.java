import java.util.Scanner;

public class Mittelwert 
{

	public static void main(String[] args) 
    {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double  x 			= 2.0;
      double  sum 			= 0;
      int     count 		= 0;
      char    input 		= ' ';
      boolean weiter    	= true;
      
      Scanner sc 			= new Scanner ( System.in );
      
      while ( weiter )
      {
    	 System.out.println( "1Bitte Zahl eingeben: " );
    	 x = sc.nextInt();
    	 count++;
    	 sum += x;
    	 
    	 System.out.println( "Weitere Zahl eingeben (j f�r ja)" );
    	 input = sc.next().charAt(0);
    	 weiter = false;
    	 if ( input == 'j' )
    	 {
    		 weiter = true;
    	 }
    	 
    	 System.out.println( "\n" );
      }
      
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      
      double mittelwert = berechneMittelwert ( sum, count );
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert der Eingabe ist " + mittelwert);
   }
   
   public static double berechneMittelwert ( double sum, int count )
   {	   
	   // (V)
	   return sum / count;
   }
   
   
   
}
