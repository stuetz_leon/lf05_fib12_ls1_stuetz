package raumschiffSchmaumschiff;

import java.util.ArrayList;
import java.util.List;

/**
 * (int photonentorpedoAnzahl, int energieversorgungInProzent, int
 * schutzschildeInProzent, int huelleInprozent, int
 * lebenserhaltungssystemeInProzent, String name, int androidenAnzahl,
 * ArrayList<Ladung> ladungsverzeichnis, ArrayList<String> broadcastKommunkator)
 *
 * @author Leon
 *
 */
public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInprozent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private List<Ladung> ladungsverzeichnis; // wir benutzen in diesem Fall die Superklasse Liste anstelle von ArrayList
	private List<String> broadcastKommunikator;

	public Raumschiff() {

	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schutzschildeInProzent,
			int huelleInprozent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String name) {

		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schutzschildeInProzent;
		this.huelleInprozent = huelleInprozent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = name;
		this.ladungsverzeichnis = new ArrayList<Ladung>();
		this.broadcastKommunikator = new ArrayList<String>();
	}

	public void addLadung(Ladung neueLadung) {

		for (final Ladung aktuelleLadung : ladungsverzeichnis) {
			if (aktuelleLadung.getBezeichnung() == neueLadung.getBezeichnung()) {
				aktuelleLadung.setMenge(aktuelleLadung.getMenge() + neueLadung.getMenge());
				return;
			}
		}
		this.ladungsverzeichnis.add(neueLadung);
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("\nRaumschiff " + this.schiffsname + "\nphotonentorpedoAnzahl="
				+ photonentorpedoAnzahl
				+ "\n energieversorgungInProzent="
				+ energieversorgungInProzent + "\n schildeInProzent=" + schildeInProzent + "\n huelleInprozent="
				+ huelleInprozent + "\n lebenserhaltungssystemeInProzent=" + lebenserhaltungssystemeInProzent
				+ "\n androidenAnzahl=" + androidenAnzahl + "\n");


		for (final Ladung ladung : ladungsverzeichnis) {
			builder.append(ladung.toString() + "\n");
		}

		for (final String string : broadcastKommunikator) {
			builder.append(string + "\n");
		}

		return builder.toString();
	}

	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		broadcastKommunikator.add(message);
	}

	public void ladungsverzeichnisAusgeben() {
		for (final Ladung ladung : ladungsverzeichnis) {
			System.out.println(ladung.toString());
		}
	}

	public void photonentorpedosSchiessen(Raumschiff zielRaumschiff) {
		if (photonentorpedoAnzahl <= 0) {
			broadcastKommunikator.add("-=*Click*=-");
		} else {
			photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(zielRaumschiff);
		}
	}

	public List<String> eintraegeLogbuchZurueckgeben() {
		return this.broadcastKommunikator;
	}

	private void treffer(Raumschiff zielRaumschiff) {
		nachrichtAnAlle(zielRaumschiff.getSchiffsname() + " wurde getroffen");

		zielRaumschiff.setSchildeInProzent(getSchildeInProzent() - 50);

		if (zielRaumschiff.getSchildeInProzent() <= 0) {
			zielRaumschiff.setSchildeInProzent(0);
			zielRaumschiff.setHuelleInprozent(getHuelleInprozent() - 50);
			zielRaumschiff.setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);

			if (zielRaumschiff.getHuelleInprozent() <= 0 && zielRaumschiff.getEnergieversorgungInProzent() <= 0) {
				zielRaumschiff.setLebenserhaltungssystemeInProzent(0);
				nachrichtAnAlle(zielRaumschiff.getSchiffsname() + "Lebenserhaltungssysteme von "
						+ zielRaumschiff.getSchiffsname() + "vollständig vernichtet");
			}
		}
	}


	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInprozent() {
		return huelleInprozent;
	}

	public void setHuelleInprozent(int huelleInprozent) {
		this.huelleInprozent = huelleInprozent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;

	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String name) {
		this.schiffsname = name;
	}

}