package raumschiffSchmaumschiff;

public class raumschiffSchmaumschiff {

	public static void main(String[] args) {

		final Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");

		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

		final Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Kaazara");
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote matierie", 2));
		romulaner.addLadung(new Ladung("Borg-Schrott", 50));

		final Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));
	}
}
