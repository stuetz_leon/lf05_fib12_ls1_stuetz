import java.util.*;
public class Fahrsimulator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double 	v 		= 0.0	;
		double 	dv				;
		
		Scanner sc = new Scanner( System.in );
		
		while ( true )
		{
			System.out.println( "\nGeschwindigkeit: " + v + "\nWas nun? " );
			dv = sc.nextDouble();
			v = beschleunige(v, dv);
		}
	}

	public static double beschleunige (double v, double dv)
	{
		v = v + dv;
		if		 ( v > 130) { v = 130; }
		else if	 ( v <   0) { v =   0; }
		return v;
	}

		
}
