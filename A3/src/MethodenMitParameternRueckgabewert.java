
public class MethodenMitParameternRueckgabewert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static double reihenschaltung(double r1, double r2)
	{
		// Berechnet den Ersatzwiderstand der Reihenschaltung aus r1 und r2
		return r1 + r2;
	}
	
	public static double parallelschaltung(double r1, double r2)
	{
		// Berechnet den Ersatzwiderstand der Parallelschaltung aus r1 und r2
		return ( r1 * r2 ) / ( r1 + r2 );
	}
}
