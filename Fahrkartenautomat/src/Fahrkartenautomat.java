﻿import java.security.PublicKey;
import java.util.Scanner;

import javax.sound.midi.MidiUnavailableException;

class Fahrkartenautomat
{
	public static void main(String[] args)
    {	
		while ( true ) // A4.5 2.
		{
			 double zuZahlenderBetrag 	= fahrkartenBestellungErfassen();
		     double rueckgeld 			= fahrkartenBezahlen ( zuZahlenderBetrag );
		     
		     fahrkartenAusgeben ();
		     rueckgeldAusgeben  ( rueckgeld );
		}
    }
	
	public static String[] initFarhkartenBezeichnung ( int anzahl )
	{
		final String[] fahrkartenBezeichnung = new String[anzahl];
		
		fahrkartenBezeichnung[0] = "Einzelfahrschein Berlin AB";
		fahrkartenBezeichnung[1] = "Einzelfahrschein Berlin BC";
		fahrkartenBezeichnung[2] = "Einzelfahrschein Berlin ABC";
		fahrkartenBezeichnung[3] = "Kurzstrecke";		
		fahrkartenBezeichnung[4] = "Tageskarte Berlin AB";
		fahrkartenBezeichnung[5] = "Tageskarte Berlin BC";
		fahrkartenBezeichnung[6] = "Tageskarte Berlin ABC";
		fahrkartenBezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
		fahrkartenBezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
		fahrkartenBezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
		
		return fahrkartenBezeichnung;
	}
	
	public static double[] initFarhkartenPreis ( int anzahl )
	{
		final double[] fahrkartenPreis 	   = new double[anzahl];
		
		fahrkartenPreis[0] =  2.90;
		fahrkartenPreis[1] =  3.30;
		fahrkartenPreis[2] =  3.60;
		fahrkartenPreis[3] =  1.90;
		fahrkartenPreis[4] =  8.60;
		fahrkartenPreis[5] =  9.00;
		fahrkartenPreis[6] =  9.60;
		fahrkartenPreis[7] = 23.60;
		fahrkartenPreis[8] = 24.30;
		fahrkartenPreis[9] = 24.90;
		
		return fahrkartenPreis;
	}
	
	public static void displayTarife ( int anzahlTarife, String[] fahrkartenBezeichnung, double[] fahrkartenPreis )
	{
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		
		for ( int i = 0; i < anzahlTarife; i++ )
		{
			System.out.println("   [" + (i+1) +  "] " + fahrkartenBezeichnung[i] + " [" + fahrkartenPreis[i] + "] " );
		}
		System.out.println();
	}
	
	public static int leseTarifEin ( int anzahlTarife )
	{
		Scanner tastatur = new Scanner(System.in);
		
		int tarif = 0;
		while ( tarif < 1 || tarif > anzahlTarife ) 
		{
			System.out.print("Ihre Wahl: ");
			tarif = tastatur.nextInt();
			System.out.println();
			if ( tarif < 1 || tarif > anzahlTarife ) { System.out.println(" >>Falsche Eingabe<<"); } 
		}
		return ( tarif - 1 );
	}
	
	public static int leseAnzahlTicketsEin ()
	{
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Anzahl der Tickets: ");
	    int anzahlTickets 	 = tastatur.nextInt();
	    
	    if ( anzahlTickets > 10 || anzahlTickets < 1 )
	    {
	    	anzahlTickets = 1;
	    	System.out.println( "Ungültige Eingabe!" );
	    	System.out.println( "Die Anzahl der tickets wurde auf eins gesetzt." );
	    }
	    return anzahlTickets;
	}
	
	public static double fahrkartenBestellungErfassen() 
	{
		final int 	   anzahlTarife 		 = 10;
		final String[] fahrkartenBezeichnung = initFarhkartenBezeichnung ( anzahlTarife );
		final double[] fahrkartenPreis 	     = initFarhkartenPreis 		 ( anzahlTarife );

		displayTarife ( anzahlTarife, fahrkartenBezeichnung, fahrkartenPreis );
		
		int tarif 		  = leseTarifEin 		 ( anzahlTarife );
		int anzahlTickets = leseAnzahlTicketsEin ();
	    
		return fahrkartenPreis[tarif] * (double) anzahlTickets; // zu zahlender Betrag
	}
	
	public static String[] initMuenzBezeichnungArray ( int muenzArraySize )
	{
		String[] muenzBezeichnungArray = new String [ muenzArraySize ];
		
		muenzBezeichnungArray [0] = " 1 Euro";
		muenzBezeichnungArray [1] = " 2 Euro";
		muenzBezeichnungArray [2] = "50 Cent";
		muenzBezeichnungArray [3] = "20 Cent";
		muenzBezeichnungArray [4] = "10 Cent";
		muenzBezeichnungArray [5] = " 5 Cent";
		muenzBezeichnungArray [6] = " 2 Cent";
		muenzBezeichnungArray [7] = " 1 Cent";
		
		return muenzBezeichnungArray;
	}
	
	public static double[] initMuenzWertArray ( int muenzArraySize )
	{
		double[] muenzWertArray = new double  [ muenzArraySize ];
		
		muenzWertArray [0] = 1.00;
		muenzWertArray [1] = 2.00;
		muenzWertArray [2] = 0.50;
		muenzWertArray [3] = 0.20;
		muenzWertArray [4] = 0.10;
		muenzWertArray [5] = 0.05;
		muenzWertArray [6] = 0.02;
		muenzWertArray [7] = 0.01;
		
		return muenzWertArray;
	}
	
	public static void displayMuenzen ( String[] muenzBezeichnungArray )
	{
		for ( int i = 0; i < muenzBezeichnungArray.length; i++ )
		{
			System.out.printf( "[%d] %s", i+1, muenzBezeichnungArray[i] );
			System.out.println();
		}
	}
	
	public static double einwerfenMuenze ( double[] muenzWertArray, int muenzArraySize )
	{
		Scanner sc  = new Scanner ( System.in );
		
		int auswahl = 0;
		while ( auswahl < 1 || auswahl > muenzArraySize )
		{
			auswahl = sc.nextInt ();
		}
		return muenzWertArray [ auswahl - 1 ];
	}
	
	public static double fahrkartenBezahlen ( double zuZahlenderBetrag )
	{
		Scanner tastatur = new Scanner(System.in);
		
		final int muenzArraySize 	    = 8;
		String[]  muenzBezeichnungArray = initMuenzBezeichnungArray ( muenzArraySize );
		double[]  muenzWertArray 	    = initMuenzWertArray 	    ( muenzArraySize );
		
		double nochZuZahlen = zuZahlenderBetrag;
		
	    while ( nochZuZahlen > 0 )
	       {
	    	   System.out.printf 			   ( "Noch zu zahlen: " + "%.2f Euro\n", nochZuZahlen );
	    	   displayMuenzen 	 			   ( muenzBezeichnungArray 							  );
	    	   nochZuZahlen -= einwerfenMuenze ( muenzWertArray, muenzArraySize 				  );
	       }
	    return -nochZuZahlen;
	}
	
	public static void fahrkartenAusgeben () 
	{
		System.out.println("\nFahrschein wird ausgegeben"); 
      
	    for (int i = 0; i < 8; i++)
	    {
	       System.out.print("=");
	       warte(50);
	    }
	    System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben (double rueckgeld) 
	{
		if(rueckgeld > 0.0)
		{
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", rueckgeld );
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while(rueckgeld >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
		        rueckgeld -= 2.0;
	        }
	        while(rueckgeld >= 1.0) // 1 EURO-Münzen
            {
	        	muenzeAusgeben(1, "EURO");
	        	rueckgeld -= 1.0;
            }
	        while(rueckgeld >= 0.5) // 50 CENT-Münzen
	        {
	        	muenzeAusgeben(50, "Cent");
	        	rueckgeld -= 0.5;
	        }
	           while(rueckgeld >= 0.2) // 20 CENT-Münzen
	           {
	        	  muenzeAusgeben(20, "Cent");
	 	          rueckgeld -= 0.2;
	           }
	           while(rueckgeld >= 0.1) // 10 CENT-Münzen
	           {
	        	  muenzeAusgeben(10, "Cent");
	        	  rueckgeld -= 0.1;
	           }
	           while(rueckgeld >= 0.05)// 5 CENT-Münzen
	           {
	        	  muenzeAusgeben(5, "Cent");
	 	          rueckgeld -= 0.05;
	           }
	       }
	    	  System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt."); 
	    	  warte(1000);
	    	  System.out.println("\n\n");
	}
	
	static void warte ( int millisekunde ) 
	{
		try 
		{
			Thread.sleep( millisekunde );
		} 
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
	static void muenzeAusgeben(int betrag, String einheit)
	{
		System.out.println ( betrag + " " + einheit );
	}
}


/*
	5. Für die Variable Tickets habe ich den Datentyp Integer gewählt, da man nur ganzzahlige en von Tickets haben kann
	6. Bei der Berechnung von  * einzelpreis wird der Integer  mit dem Double einzelreis multipliziert. 
		Dabei wird  automatisch von Java in den Datentyp Double konvertiert


*/